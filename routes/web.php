<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/categories','App\Http\Controllers\Api\ApiController@getCategories');
Route::get('/tags','App\Http\Controllers\Api\ApiController@getTags');
Route::get('/accounts','App\Http\Controllers\Api\ApiController@getAccountNames');
Route::post('/expense','App\Http\Controllers\Api\ApiController@createExpense');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
