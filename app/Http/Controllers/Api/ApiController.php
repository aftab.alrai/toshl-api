<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;

class ApiController extends Controller
{
    /*Fetch categories from Toshl.
    you can call getCategories function by calling the route localhost:800/categories
    */
    public function getCategories()
    {
        $endPoint = 'https://api.toshl.com/categories';
        $url = $endPoint;

        $ch = curl_init();
        $headers = array('Authorization: Bearer 8c7533ad-8089-46c0-b8ba-b37032971f21f0684662-edf0-4b5c-8d43-88684f53c9d9','Content-Type: application/json');
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);
        $result = json_decode($result, TRUE);
        curl_close($ch);
        Log::info('results');
        Log::info($url);
        Log::info($result);
        return $result;
    }

    /*Fetch Tags from Toshl.
    you can call getTags function by calling the route localhost:800/tags
    */
    public function getTags()
    {
        $endPoint = 'https://api.toshl.com/tags';
        $url = $endPoint;

        $ch = curl_init();
        $headers = array('Authorization: Bearer 8c7533ad-8089-46c0-b8ba-b37032971f21f0684662-edf0-4b5c-8d43-88684f53c9d9','Content-Type: application/json');
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);
        $result = json_decode($result, TRUE);
        curl_close($ch);
        Log::info('results');
        Log::info($url);
        Log::info($result);
        return $result;
    }

    /*Fetch Account Names from Toshl.*/
    public function getAccountNames()
    {
        $endPoint = 'https://api.toshl.com/accounts';
        $url = $endPoint;

        $ch = curl_init();
        $headers = array('Authorization: Bearer 8c7533ad-8089-46c0-b8ba-b37032971f21f0684662-edf0-4b5c-8d43-88684f53c9d9','Content-Type: application/json');
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);
        $result = json_decode($result, TRUE);
        curl_close($ch);
        Log::info('results');
        Log::info($url);
        Log::info($result);
        return $result;
    }

    /*Create expense entry.*/
    public function createExpense()
    {
        
        $endPoint = 'https://api.toshl.com/entries';
        $url = $endPoint;

        $ch = curl_init();
        $headers = array('Authorization: Bearer 8c7533ad-8089-46c0-b8ba-b37032971f21f0684662-edf0-4b5c-8d43-88684f53c9d9','Content-Type: application/json');

        $data = [
        'amount' => '-200',
        'currency' => [
            'code' => 'USD'
        ],
        'date' => '2020-09-11',
        'account' => '3621896',
        'category' => '64821871'
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        $result = json_decode($result, TRUE);
        curl_close($ch);
        Log::info('results');
        Log::info($url);
        Log::info($result);
        return $result;
    }
}
